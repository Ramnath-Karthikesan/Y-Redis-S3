import * as Y from 'yjs'
import * as random from 'lib0/random'
import * as promise from 'lib0/promise'
import * as minio from 'minio'
import * as env from 'lib0/environment'
import * as number from 'lib0/number'
import { S3Client, PutObjectCommand, GetObjectCommand, ListObjectsV2Command, DeleteObjectsCommand } from '@aws-sdk/client-s3';

/**
 * @typedef {import('../storage.js').AbstractStorage} AbstractStorage
 */


/**
 * @todo perform some sanity checks here before starting (bucket exists, ..)
 * @param {string} bucketName
 */
export const createS3Storage = (bucketName) => {
  const endPoint = env.ensureConf('s3-endpoint')
  const port = number.parseInt(env.ensureConf('s3-port'))
  const useSSL = !['false', '0'].includes(env.getConf('s3-ssl') || 'false')
  const accessKey = env.ensureConf('s3-access-key')
  const secretKey = env.ensureConf('s3-secret-key')
  return new S3Storage(bucketName, {
    endPoint,
    port,
    useSSL,
    accessKey,
    secretKey
  })
}

/**
 * @param {string} folderName
 * @param {string} room
 * @param {string} docid
 */
export const encodeS3ObjectName = (folderName, room, docid, r = random.uuidv4()) => `${encodeURIComponent(folderName)}/${encodeURIComponent(room)}/${encodeURIComponent(docid)}/${r}`


/**
 * @typedef {Object} S3StorageConf
 * @property {string} S3StorageConf.endPoint
 * @property {number} S3StorageConf.port
 * @property {boolean} S3StorageConf.useSSL
 * @property {string} S3StorageConf.accessKey
 * @property {string} S3StorageConf.secretKey
 */

/**
 * @param {any} stream
 * @return {Promise<Buffer>}
 */
const readStream = stream => promise.create((resolve, reject) => {
  /**
   * @type {Array<Buffer>}
   */
  const chunks = []
  stream.on('data', /** @param {Buffer} chunk */ chunk => chunks.push(Buffer.from(chunk)))
  stream.on('error', reject)
  stream.on('end', () => resolve(Buffer.concat(chunks)))
})


/**
 * @implements {AbstractStorage}
 */
export class S3Storage {
    /**
    * @param {string} bucketName
    * @param {S3StorageConf} conf
    */

    constructor(bucketName, { endPoint, port, useSSL, accessKey, secretKey }){
        this.bucketName = bucketName
        this.client = new S3Client({
            credentials: {
                accessKeyId: accessKey,
                secretAccessKey: secretKey
            },
            region: env.ensureConf('s3-region')
        });
    }


    /**
    * @param {string} room
    * @param {string} docname
    * @param {Y.Doc} ydoc
    * @returns {Promise<void>}
    */
    async persistDoc (room, docname, ydoc) {
      const s3Folder = env.ensureConf("s3-folder")
      const objectName = encodeS3ObjectName(s3Folder, room, docname)
      const data = Buffer.from(Y.encodeStateAsUpdateV2(ydoc));
      console.log("saving ydoc : ", objectName)
      await this.client.send(new PutObjectCommand({
        Bucket: this.bucketName,
        Key: objectName,
        Body: data
      }));
    }


    /**
     * @param {string} room
     * @param {string} docname
     * @return {Promise<{ doc: Uint8Array, references: Array<any> } | null>}
     */
    async retrieveDoc (room, docname) {
    //   const objNames = await this.client.listObjectsV2(this.bucketName, encodeS3ObjectName(room, docname, ''), true).toArray()
        const s3Folder = env.ensureConf("s3-folder")
        const objNames = await this.client.send(new ListObjectsV2Command({
            Bucket: this.bucketName,
            Prefix: `${s3Folder}/${room}/${docname}`
        }));
          
        const references = objNames.Contents?.map(obj => obj.Key) ?? [];
        if (references.length === 0) {
            return null
        }

        let updates = await Promise.all(references.map(async ref => {
            const data = await this.client.send(new GetObjectCommand({
              Bucket: this.bucketName,
              Key: ref
            }));
            if (data.Body) {
              return readStream(data.Body);
            } else {
              throw new Error('Body is undefined');
            }
        }));
          

        return { doc: Y.mergeUpdatesV2(updates), references}
    }

    
    /**
     * @param {string} room
     * @param {string} docname
     * @return {Promise<Uint8Array|null>}
     */
    async retrieveStateVector (room, docname) {
      const r = await this.retrieveDoc(room, docname)
      return r ? Y.encodeStateVectorFromUpdateV2(r.doc) : null
    }


    /**
     * @param {string} _room
     * @param {string} _docname
     * @param {Array<string>} storeReferences
     * @return {Promise<void>}
     */
    async deleteReferences (_room, _docname, storeReferences) {
      const params = {
        Bucket: this.bucketName,
        Delete: {
            Objects: storeReferences.map(key => ({ Key: key })),
            Quiet: true // Set to true if you don't want error messages for keys that are not deleted
        }
      };
      await this.client.send(new DeleteObjectsCommand(params));
    }

    async destroy () {
    }
}